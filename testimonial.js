const testimonial = [
  {
    image: 'image-tanya.jpg',
    info: '“ I’ve been interested in coding for a while but never taken the jump, until now. I couldn’t recommend this course enough. I’m now in the job of my dreams and so excited about the future. ”',
    name: 'Tanya Sinclair',
    job: 'UX Engineer',
  },
  {
    image: 'image-john.jpg',
    info: '“If you want to lay the best foundation possible I’d recommend taking this course. The depth the instructors go into is incredible. I now feel so confident about starting up as a professional developer. ”',
    name: 'John Tarkpor',
    job: 'Junior Front-end Developer',
  },
];

const $prev = document.getElementById('prev');
const $next = document.getElementById('next');
const $image = document.getElementById('image');
const $info = document.querySelector('.testimonials__info');
const $name = document.querySelector('.testimonials__title');
const $job = document.querySelector('.testimonials__job');

const slider = () => {
  let index = 0;
  $prev.addEventListener('click', (event) => {
    index--;
    if (index < 0) {
      index = testimonial.length - 1;
    }
    $image.src = testimonial[index].image;
    $info.textContent = testimonial[index].info;
    $name.textContent = testimonial[index].name;
    $job.textContent = testimonial[index].job;
  });
  $next.addEventListener('click', () => {
    index++;
    if (index >= testimonial.length) {
      index = 0;
    }
    $image.src = testimonial[index].image;
    $info.textContent = testimonial[index].info;
    $name.textContent = testimonial[index].name;
    $job.textContent = testimonial[index].job;
  });
};
export { slider };
